namespace Minutz.Core.Api.Models.Entities
{
    public static class RoleTypes
    {
        public const string User = "User";
        public const string Guest = "Guest";
        public const string Admin = "Admin";
    }
}