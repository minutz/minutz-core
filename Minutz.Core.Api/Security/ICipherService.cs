namespace Minutz.Core.Api.Security
{
    public interface ICipherService
    {
        string Encrypt(string input);
        
        string Decrypt(string cipherText);
    }
}