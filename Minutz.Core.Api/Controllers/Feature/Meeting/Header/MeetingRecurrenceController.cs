using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Recurrence;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Header
{
    
    public class MeetingRecurrenceController : BaseController
    {
        private readonly IMinutzRecurrenceService _minutzRecurrenceService;

        public MeetingRecurrenceController(IMinutzRecurrenceService minutzRecurrenceService)
        {
            _minutzRecurrenceService = minutzRecurrenceService;
        }

        [Authorize]
        [HttpPost("api/feature/header/recurrence", Name = "Update Meeting recurrence type")]
        public IActionResult UpdateMeetingLocationResult(string id, int recurrence, string instanceId)
        {
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = AuthUser.InfoResponse.InstanceId;
            var result = _minutzRecurrenceService.Update(id, recurrence, AuthUser.InfoResponse, schema);
            if (result.Condition)
            {
                return Ok();
            }
            return StatusCode(result.Code, result.Message);
        }
    }
}