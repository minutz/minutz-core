﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Feature.Action;

namespace Minutz.Core.Api.Controllers.Feature.Meeting.Action
{
    
    public class MinutzActionController : BaseController
    {
        private readonly IMinutzActionService _minutzActionService;

        public MinutzActionController(IMinutzActionService minutzActionService)
        {
            _minutzActionService = minutzActionService;
        }

        
        [HttpGet("api/feature/actions", Name = "Get the actions for a meeting")]
        public IActionResult GetMeetingActionsResult(string meetingId, string instanceId)
        {
            if (string.IsNullOrEmpty(meetingId))
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
            var result = _minutzActionService.GetMeetingActions
                (Guid.Parse(meetingId), AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Actions) : StatusCode(result.Code, result.Message);
        }

        [Authorize]
        [HttpPut("api/feature/action/quick", Name = "Quick create action")]
        public IActionResult QuickCreateActionResult([FromBody]QuickActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.QuickCreate
                (request.MeetingId, request.ActionText,request.Order, AuthUser.InfoResponse, schema);
            return result.Condition ? Ok(result.Action) : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/action/update", Name = "Update action text")]
        public IActionResult UpdateActionUpdateTextResult([FromBody]UpdateActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            string text = request.Value as string;
            if(string.IsNullOrEmpty(text)) return StatusCode(401, "Request is missing complete value for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.UpdateActionText
                (request.Id, text , AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/action/title", Name = "Update action title")]
        public IActionResult UpdateActionUpdateTitleResult([FromBody]UpdateActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            string text = request.Value as string;
            if(string.IsNullOrEmpty(text)) return StatusCode(401, "Request is missing complete value for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.UpdateActionText
                (request.Id, text , AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/action/complete", Name = "Update action complete status")]
        public IActionResult UpdateActionCompleteResult([FromBody]UpdateActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            bool? complete = request.Value as bool?;
            if(complete == null) return StatusCode(401, "Request is missing complete value for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.UpdateActionComplete
                (request.Id, (bool)complete , AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/action/order", Name = "Update action order")]
        public IActionResult UpdateActionOrderResult([FromBody]UpdateActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            int? order = request.Value as int?;
            if(order == null) return StatusCode(401, "Request is missing order value for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.UpdateActionOrder
                (request.Id, (int)order , AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/action/assigned", Name = "Update action assigned attendee")]
        public IActionResult UpdateActionAssignedAttendeeResult([FromBody]UpdateActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            string assignedAttendee = request.Value as string;
            if(string.IsNullOrEmpty(assignedAttendee)) return StatusCode(401, "Request is missing assigned attendee value for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.UpdateActionAssignedAttendee
                (request.Id, assignedAttendee , AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/action/due", Name = "Update action due date")]
        public IActionResult UpdateActionDueDateResult([FromBody]UpdateActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            DateTime? due = request.Value as DateTime?;
            if(due == null) return StatusCode(401, "Request is missing due date value for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.UpdateActionDueDate
                (request.Id, ((DateTime)due).ToLocalTime() , AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpPost("api/feature/action/raised", Name = "Update action raised date")]
        public IActionResult UpdateActionRaisedDateResult([FromBody]UpdateActionRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            DateTime? due = request.Value as DateTime?;
            if(due == null) return StatusCode(401, "Request is missing due date value for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(request.InstanceId)) schema = request.InstanceId;
            var result = _minutzActionService.UpdateActionRaisedDate
                (request.Id, ((DateTime) due).ToLocalTime(), AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
        
        [Authorize]
        [HttpDelete("api/feature/action", Name = "Delete action")]
        public IActionResult DeleteActionResult(Guid id, string instanceId)
        {
            if (!ModelState.IsValid)
                return StatusCode(401, "Request is missing values for the request");
            var schema = AuthUser.InfoResponse.InstanceId;
            if (!string.IsNullOrEmpty(instanceId)) schema = instanceId;
            var result = _minutzActionService.Delete
                (id, AuthUser.InfoResponse, schema);
            return result.Condition ? (IActionResult) Ok() : StatusCode(result.Code, result.Message);
        }
    }
}