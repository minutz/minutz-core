using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Attendee;
using Minutz.Core.Api.Feature.Invitation;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Notification;
using Minutz.Core.Feature.Person;


namespace Minutz.Core.Api.Controllers.Feature.Invite
{
    public class InviteController : BaseController
    {
        private readonly IConnectionStringService _applicationSetting;
        private readonly IGetMeetingService _getMeetingService;
        private readonly IInvitationService _invitationService;
        private readonly INotificationService _notificationService;
        private readonly IMeetingAttendeeRepository _attendeeRepository;

        public InviteController(IConnectionStringService applicationSetting,
                                IGetMeetingService getMeetingService,
                                IInvitationService invitationService,
                                INotificationService notificationService,
                                IMeetingAttendeeRepository attendeeRepository)
        {
            _applicationSetting = applicationSetting;
            _getMeetingService = getMeetingService;
            _invitationService = invitationService;
            _notificationService = notificationService;
            _attendeeRepository = attendeeRepository;
        }

        /// <summary>
        /// Invite Attendee.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /Invite
        ///     {
        ///        "id": 1,
        ///        "name": "Item1",
        ///        "isComplete": true
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>A newly-created TodoItem</returns>
        /// <response code="201">Returns the newly-created item</response>
        /// <response code="400">If the item is null</response>
        [Authorize]
        [HttpPost("api/feature/attendee/invite", Name = "Invite Attendee for meeting")]
        public IActionResult InviteAttendeeResult([FromBody] MeetingAttendee attendee)
        {
            var validate = attendee.Validate();
            if (!validate.Condition)
                return new BadRequestObjectResult(validate.Message);

            attendee.PersonIdentity = attendee.Email;
            attendee.Role = "Invited";
            attendee.Status = "Invited";

            var meeting = _getMeetingService.GetMeeting(AuthUser.InfoResponse.InstanceId, attendee.ReferenceId);
            if (!meeting.Condition)
            {
                return BadRequest("There was a issue getting the meeting information.");
            }

            var inviteRecords = _invitationService.InviteUser(AuthUser.InfoResponse, attendee, meeting.Meeting);
            if (!inviteRecords.Condition)
            {
                return StatusCode(inviteRecords.Code, inviteRecords.Message);
            }

            var person = new Person
            {
                Email = attendee.Email,
                FullName = attendee.Name
            };

            // var notificationResult = _notificationService.SendInstanceInvitation(person, meeting.Meeting.InstanceId, meeting.Meeting.Name);
            // if (notificationResult.Condition)
            // {
            //     return Ok();
            // }
            return Ok();
            //return StatusCode(notificationResult.Code, notificationResult.Message);
        }

        [Authorize]
        [HttpPost("api/feature/attendee/invites", Name = "Invite Attendees for meeting")]
        public IActionResult SendInvitesResult(Guid meetingId)
        {
            if (meetingId == Guid.Empty)
                return new BadRequestObjectResult("Meeting id is incorrect");
            var meeting = _getMeetingService.GetMeeting(AuthUser.InfoResponse.InstanceId, meetingId);
            var instanceConnectionString = _applicationSetting.CreateConnectionString( AuthUser.InfoResponse.InstanceId, _applicationSetting.GetInstancePassword(AuthUser.InfoResponse.InstanceId));
            var master = _applicationSetting.CreateConnectionString();

            var attendees =
                _attendeeRepository.GetMeetingAttendees(meetingId, AuthUser.InfoResponse.InstanceId, instanceConnectionString,
                    master);
            foreach (var meetingAttendee in attendees)
            {
                var person = new Person
                {
                    Email = meetingAttendee.Email,
                    FullName = meetingAttendee.Name
                };
               // var notificationResult = _notificationService.SendInstanceInvitation(person, meeting.Meeting.InstanceId, meeting.Meeting.Name);
            }
            return Ok();
        }
        
        [Authorize]
        [HttpPost("api/feature/attendee/invitation", Name = "Invite Attendees for meeting with message body")]
        public IActionResult SendInvitesWithMessageResult([FromBody] InviteRequestMessage request)
        {
            if (request.MeetingId == Guid.Empty)
                return new BadRequestObjectResult("Meeting id is incorrect");
            var meeting = _getMeetingService.GetMeeting(AuthUser.InfoResponse.InstanceId, request.MeetingId);
            var instanceConnectionString = _applicationSetting.CreateConnectionString(AuthUser.InfoResponse.InstanceId, _applicationSetting.GetInstancePassword(AuthUser.InfoResponse.InstanceId));
            var master = _applicationSetting.CreateConnectionString();

            var attendees =
                _attendeeRepository.GetMeetingAttendees(request.MeetingId, AuthUser.InfoResponse.InstanceId, instanceConnectionString,
                    master);
            attendees.Add(new MeetingAttendee
                          {
                              Email = AuthUser.InfoResponse.Email,
                              Name = AuthUser.InfoResponse.Name,
                              Role = AuthUser.InfoResponse.Role,
                              PersonIdentity = AuthUser.InfoResponse.Email
                          });
            foreach (var meetingAttendee in attendees)
            {
                var person = new Person
                {
                    Email = meetingAttendee.Email,
                    FullName = meetingAttendee.Name
                };
               // var notificationResult = _notificationService.SendInstanceInvitation(person, meeting.Meeting.InstanceId, meeting.Meeting.Name);
            }
            return Ok();
        }
        
        [Authorize]
        [HttpPost("api/feature/attendee/minutes", Name = "Send minutes for meeting")]
        public IActionResult SendMinutesResult(Guid meetingId)
        {
            if (meetingId == Guid.Empty)
                return new BadRequestObjectResult("Meeting id is incorrect");
            var meeting = _getMeetingService.GetMeeting(AuthUser.InfoResponse.InstanceId, meetingId);
            var instanceConnectionString = _applicationSetting.CreateConnectionString(AuthUser.InfoResponse.InstanceId, _applicationSetting.GetInstancePassword(AuthUser.InfoResponse.InstanceId));
            var master = _applicationSetting.CreateConnectionString();

            var attendees =
                _attendeeRepository.GetMeetingAttendees(meetingId, AuthUser.InfoResponse.InstanceId, instanceConnectionString,
                    master);
            foreach (var meetingAttendee in attendees)
            {
                var person = new Person
                {
                    Email = meetingAttendee.Email,
                    FullName = meetingAttendee.Name
                };
                // var notificationResult = _notificationService.SendInstanceInvitation(person, meeting.Meeting.InstanceId, meeting.Meeting.Name);
            }
            return Ok();
        }
    }
}