using System;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Decision
{
    public interface IMinutzDecisionService
    {
        DecisionMessage GetMeetingDecisions(Guid meetingId, AuthRestModel user, string schema);

        DecisionMessage QuickDecisionCreate(Guid meetingId, string decisionText, int order, AuthRestModel user, string schema);

        DecisionMessage UpdateDecision(Guid meetingId, MinutzDecision decision, AuthRestModel user, string schema);

        MessageBase DeleteDecision(Guid decisionId, AuthRestModel user, string schema);
    }
}