using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Contracts
{
    public interface IApplicationManagerService
    {
        (bool condition, string message) StartFullVersion(AuthRestModel user);

        (bool condition, string message) ResetAccount(AuthRestModel user);
    }
}