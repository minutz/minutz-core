using System;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Status
{
    public interface IMeetingStatusService
    {
        MessageBase UpdateMeetingStatus(Guid meetingId, string status, AuthRestModel user, string schema);
    }
}