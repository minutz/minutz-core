using System;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Note
{
    public interface IMinutzNoteRepository
    {
        NoteMessage GetNoteCollection(Guid meetingId, string schema, string connectionString);

        NoteMessage QuickCreateNote(Guid meetingId, string noteText, int order, string schema, string connectionString);

        NoteMessage UpdateNote(Guid meetingId, MeetingNote note, string schema, string connectionString);

        MessageBase DeleteNote(Guid noteId, string schema, string connectionString);
    }
}