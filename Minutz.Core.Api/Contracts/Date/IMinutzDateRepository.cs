using System;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Date
{
    public interface IMinutzDateRepository
    {
        MessageBase Update(string meetingId, DateTime date,  string schema, string connectionString);
    }
}