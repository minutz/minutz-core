using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Time
{
    public interface IMinutzTimeRepository
    {
        MessageBase Update(string meetingId, string time, string schema, string connectionString);
    }
}