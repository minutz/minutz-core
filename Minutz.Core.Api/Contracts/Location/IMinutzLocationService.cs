using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Contracts.Location
{
    public interface IMinutzLocationService
    {
        MessageBase Update(string meetingId, string location, AuthRestModel user, string schema);
    }
}