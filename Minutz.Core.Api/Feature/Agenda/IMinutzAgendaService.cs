using System;
using Minutz.Core.Api.Feature.Agenda;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Feature.Agenda
{
    public interface IMinutzAgendaService
    {
        AgendaMessage GetMeetingAgendaCollection(Guid meetingId, AuthRestModel user, string schema);

        MessageBase UpdateComplete(Guid agendaId, bool isComplete, AuthRestModel user, string schema);

        MessageBase UpdateOrder(Guid agendaId, int order, AuthRestModel user, string schema);

        MessageBase UpdateDuration(Guid agendaId, int duration, AuthRestModel user, string schema);

        MessageBase UpdateTitle(Guid agendaId, string title, AuthRestModel user, string schema);

        MessageBase UpdateText(Guid agendaId, string text, AuthRestModel user, string schema);

        MessageBase UpdateAssignedAttendee(Guid agendaId, string attendeeEmail, AuthRestModel user, string schema);

        AgendaMessage QuickCreate(string meetingId, string agendaTitle, int order, AuthRestModel user, string schema);

        MessageBase Delete(Guid agendaId, AuthRestModel user, string schema);
    }
}