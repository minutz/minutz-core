using System;

namespace Minutz.Core.Api.Feature.Invitation
{
    public static class InviteRequestExtensions
    {
        public static InviteValidateResult Validate(this Models.Entities.MeetingAttendee model)
        {
            if (model.ReferenceId == Guid.Empty)
            {
                return new InviteValidateResult{ Message = "Please provide a valid meetingId", Condition = false};
            }
            if (string.IsNullOrEmpty(model.Email))
            {
                return new InviteValidateResult { Message = "Please provide a valid email address", Condition = false};
            }
            return string.IsNullOrEmpty(model.Name) 
                ? new InviteValidateResult{ Message = "Please provide a valid name.", Condition = false} 
                : new InviteValidateResult {Condition = true, Message = "Valid"};
        }
    }
}