namespace Minutz.Core.Api.Feature.Invitation
{
    public class InviteValidateResult
    {
        /// <summary>
        /// The Boolean result condition
        /// </summary>
        public bool Condition { get; set; }

        /// <summary>
        /// The logical message that is returned
        /// </summary>
        public string Message { get; set; }
    }
}