using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Feature.EMail;
using Minutz.Core.Api.Feature.Meeting;
using Minutz.Core.Api.Feature.MeetingAttendee;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;
using Minutz.Core.Api.User;

namespace Minutz.Core.Api.Feature.Invitation
{
    public class InvitationService : IInvitationService
    {
        private readonly IEmailValidationService _emailValidationService;
        private readonly IMinutzAvailabilityRepository _availabilityRepository;
        private readonly IMinutzAttendeeRepository _attendeeRepository;
        private readonly IUserRepository _userRepository;
        private readonly IConnectionStringService _applicationSetting;

        public InvitationService(IEmailValidationService emailValidationService,
            IMinutzAvailabilityRepository availabilityRepository,
            IMinutzAttendeeRepository attendeeRepository,
            IUserRepository userRepository,
            IConnectionStringService applicationSetting)
        {
            _emailValidationService = emailValidationService;
            _availabilityRepository = availabilityRepository;
            _attendeeRepository = attendeeRepository;
            _userRepository = userRepository;
            _applicationSetting = applicationSetting;
        }

        /// <summary>
        /// Invite a attendee
        /// Validate the attendee is new, alternatively update reference to the new instance if required
        /// </summary>
        /// <param name="user" typeof="AuthRestModel">The current user / owner</param>
        /// <param name="invitee" typeof="MeetingAttendee">The new attendee</param>
        /// <param name="meeting" typeof="Minutz.Models.Entities.Meeting">The meeting entity for the invitation</param>
        /// <returns typeof="MessageBase">Result if was successful</returns>
        public MessageBase InviteUser(AuthRestModel user, Models.Entities.MeetingAttendee invitee, Meeting.Meeting meeting)
        {
            var validEmail = _emailValidationService.Valid(invitee.Email);
            if (!validEmail)
                return new MessageBase {Condition = false, Message = $"{invitee.Email} is a invalid email address."};
            if (meeting.Id == Guid.Empty || string.IsNullOrEmpty(meeting.Name))
                return new MessageBase
                {
                    Condition = false,
                    Message = "Meeting is invalid."
                };

            var instanceConnectionString = _applicationSetting.CreateConnectionString( user.InstanceId,
                _applicationSetting.GetInstancePassword(user.InstanceId));

            var masterConnectionString = _applicationSetting.CreateConnectionString();

            var userValidation = _userRepository.CheckIfNewUser(invitee.Email, invitee.ReferenceId.ToString(),
                user.InstanceId, instanceConnectionString, masterConnectionString);
            switch (userValidation.Code)
            {
                case 1:
                    //create person
                    var createPersonResult = _userRepository.CreatePerson(invitee, masterConnectionString);
                    if (!createPersonResult.Condition)
                    {
                        return new MessageBase
                        {
                            Condition = createPersonResult.Condition, Message = createPersonResult.Message,
                            Code = createPersonResult.Code
                        };
                    }

                    var createPersonAvailableResult =
                        _availabilityRepository.CreateAvailableAttendee(invitee, user.InstanceId,
                            instanceConnectionString);
                    if (!createPersonAvailableResult.Condition)
                    {
                        var message =
                            $"Person was created but there was a issue with creating available attendee. \n {createPersonAvailableResult.Message}";
                        return new MessageBase
                        {
                            Condition = createPersonAvailableResult.Condition,
                            Message = message,
                            Code = createPersonAvailableResult.Code
                        };
                    }

                    var createPersonAttendeeResult = _attendeeRepository.AddAttendee(invitee.ReferenceId, invitee,
                        user.InstanceId, instanceConnectionString);
                    if (!createPersonAttendeeResult.Condition)
                    {
                        var message =
                            $"Person was created, and available attendee was created, but there was a issue with adding to meeting attendee. \n {createPersonAttendeeResult.Message}";
                        return new MessageBase
                        {
                            Condition = createPersonAttendeeResult.Condition,
                            Message = message,
                            Code = createPersonAttendeeResult.Code
                        };
                    }

                    break;
                case 2:
                    //create available
                    var createAvailableResult =
                        _availabilityRepository.CreateAvailableAttendee(invitee, user.InstanceId,
                            instanceConnectionString);
                    if (!createAvailableResult.Condition)
                    {
                        return new MessageBase
                        {
                            Condition = createAvailableResult.Condition, Message = createAvailableResult.Message,
                            Code = createAvailableResult.Code
                        };
                    }

                    var createAvailableAttendeeResult = _attendeeRepository.AddAttendee(invitee.ReferenceId, invitee,
                        user.InstanceId, instanceConnectionString);
                    if (!createAvailableAttendeeResult.Condition)
                    {
                        var message =
                            $"Available attendee was created, but there was a issue with adding to meeting attendee. \n {createAvailableAttendeeResult.Message}";
                        return new MessageBase
                        {
                            Condition = createAvailableAttendeeResult.Condition,
                            Message = message,
                            Code = createAvailableAttendeeResult.Code
                        };
                    }

                    break;
                case 3:
                    //create meeting attendee
                    var createAttendeeResult = _attendeeRepository.AddAttendee(invitee.ReferenceId, invitee,
                        user.InstanceId, instanceConnectionString);
                    if (!createAttendeeResult.Condition)
                    {
                        var message =
                            $"There was a issue with adding to meeting attendee. \n {createAttendeeResult.Message}";
                        return new MessageBase
                        {
                            Condition = createAttendeeResult.Condition,
                            Message = message,
                            Code = createAttendeeResult.Code
                        };
                    }

                    break;
            }

            return new MessageBase {Condition = true, Message = "Successful"};
        }

        public bool SendMeetingInvitation(Models.Entities.MeetingAttendee attendee, MeetingViewModel meeting, string instanceId)
        {
            throw new System.NotImplementedException();
        }
    }
}