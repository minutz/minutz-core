using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Action
{
    public class MinutzActionService : IMinutzActionService
    {
        private readonly IMinutzActionRepository _minutzActionRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzActionService(IMinutzActionRepository minutzActionRepository,
            IConnectionStringService applicationSetting)
        {
            _minutzActionRepository = minutzActionRepository;
            _applicationSetting = applicationSetting;
        }

        public ActionMessage GetMeetingActions(Guid meetingId, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.GetMeetingActions(meetingId, schema,instanceConnectionString);
        }

        public MessageBase UpdateActionComplete(Guid actionId, bool isComplete, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.UpdateActionComplete(actionId, isComplete, schema,
                instanceConnectionString);
        }

        public MessageBase UpdateActionText(Guid actionId, string text, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.UpdateActionText(actionId, text, schema, instanceConnectionString);
        }
        
        public MessageBase UpdateActionTitle(Guid actionId, string text, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.UpdateActionTitle(actionId, text, schema, instanceConnectionString);
        }

        public MessageBase UpdateActionAssignedAttendee(Guid actionId, string email, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.UpdateActionAssignedAttendee(actionId, email, schema,
                instanceConnectionString);
        }

        public MessageBase UpdateActionDueDate(Guid actionId, DateTime dueDate, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.UpdateActionDueDate(actionId, dueDate, schema,
                instanceConnectionString);
        }

        public MessageBase UpdateActionRaisedDate(Guid actionId, DateTime dueDate, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.UpdateActionRaisedDate(actionId, dueDate, schema,
                instanceConnectionString);
        }

        public MessageBase UpdateActionOrder(Guid actionId, int order, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.UpdateActionOrder(actionId, order, schema,
                instanceConnectionString);
        }

        public ActionMessage QuickCreate(Guid meetingId, string actionText, int order, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.QuickCreate(meetingId, actionText, order, schema,
                instanceConnectionString);
        }

        public MessageBase Delete(Guid actionId, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzActionRepository.Delete(actionId, schema, instanceConnectionString);
        }
    }
}