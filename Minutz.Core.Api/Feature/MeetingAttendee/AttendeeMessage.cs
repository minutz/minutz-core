using System.Collections.Generic;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public class AttendeeMessage: MessageBase
    {
        public Models.Entities.MeetingAttendee Attendee { get; set; }
        public List<Models.Entities.MeetingAttendee> Attendees { get; set; }
    }
}