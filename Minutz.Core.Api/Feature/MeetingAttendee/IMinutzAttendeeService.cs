using System;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.MeetingAttendee
{
    public interface IMinutzAttendeeService
    {
        AttendeeMessage GetAttendees(Guid meetingId, AuthRestModel user , string schema);
        
        AttendeeMessage AddAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee, AuthRestModel user, string schema);

        AttendeeMessage UpdateAttendee(Guid meetingId, Models.Entities.MeetingAttendee attendee, AuthRestModel user, string schema);

        MessageBase DeleteAttendee(Guid meetingId, string attendeeEmail, AuthRestModel user, string schema);
    }
}