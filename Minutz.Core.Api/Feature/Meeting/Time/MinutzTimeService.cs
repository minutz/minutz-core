using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Time;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Time
{
    public class MinutzTimeService : IMinutzTimeService
    {
        private readonly IMinutzTimeRepository _minutzTimeRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzTimeService(IMinutzTimeRepository minutzTimeRepository, IConnectionStringService applicationSetting)
        {
            _minutzTimeRepository = minutzTimeRepository;
            _applicationSetting = applicationSetting;
        }

        public MessageBase Update(string meetingId, string time, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzTimeRepository.Update(meetingId, time, user.InstanceId, instanceConnectionString);
        }
    }
}