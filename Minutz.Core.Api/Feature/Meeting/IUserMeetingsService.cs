using System;
using Minutz.Core.Api.Models.Entities;

namespace Minutz.Core.Api.Feature.Meeting
{
    public interface IUserMeetingsService
    {
        MeetingMessage Meetings(AuthRestModel user);

        MeetingMessage Meeting(AuthRestModel user, Guid meetingId, string instanceId);

        MeetingMessage CreateEmptyUserMeeting (AuthRestModel user);
    }
}