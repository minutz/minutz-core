using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Dapper;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Date;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Date
{
    public class MinutzDateRepository: IMinutzDateRepository
    {
        public MessageBase Update(string meetingId, DateTime date, string schema, string connectionString)
        {
            if (string.IsNullOrEmpty(meetingId) ||
                string.IsNullOrEmpty(schema) ||
                string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Please provide a valid meeting identifier, schema or connection string.");
            try
            {
                using (IDbConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    var sql = $"UPDATE [{schema}].[Meeting] SET Date ='{date}' WHERE Id = '{meetingId}'";
                    var data = dbConnection.Execute(sql);
                    return data == 1
                        ? new MessageBase {Code = 200, Condition = true, Message = date.ToString(CultureInfo.InvariantCulture)}
                        : new MessageBase {Code = 404, Condition = false, Message = "Could not update meeting."};
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new MessageBase {Code = 500, Condition = false, Message = e.Message};
            }
        }
    }
}