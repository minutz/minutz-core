using System;
using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Decision;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Decision
{
    public class MinutzDecisionService : IMinutzDecisionService
    {
        private readonly IMinutzDecisionRepository _decisionRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzDecisionService(IConnectionStringService applicationSetting,
                                     IMinutzDecisionRepository decisionRepository)
        {
            _decisionRepository = decisionRepository;
            _applicationSetting = applicationSetting;
        }
        
        public DecisionMessage GetMeetingDecisions(Guid meetingId, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _decisionRepository.GetDecisionCollection(meetingId, user.InstanceId, instanceConnectionString);
        }

        public DecisionMessage QuickDecisionCreate(Guid meetingId, string decisionText, int order, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _decisionRepository.QuickCreateDecision(meetingId, decisionText, order, user.InstanceId, instanceConnectionString);
        }

        public DecisionMessage UpdateDecision(Guid meetingId, MinutzDecision decision, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString( schema, _applicationSetting.GetInstancePassword(schema));
            return _decisionRepository.UpdateDecision(meetingId, decision, user.InstanceId, instanceConnectionString);
        }
        
        public MessageBase DeleteDecision(Guid decisionId, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _decisionRepository.DeleteDecision(decisionId, user.InstanceId, instanceConnectionString);
        }
    }
}