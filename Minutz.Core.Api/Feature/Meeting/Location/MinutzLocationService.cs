using Minutz.Core.Api.Contracts;
using Minutz.Core.Api.Contracts.Location;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Api.Models.Message;

namespace Minutz.Core.Api.Feature.Meeting.Location
{
    public class MinutzLocationService: IMinutzLocationService
    {
        private readonly IMinutzLocationRepository _minutzLocationRepository;
        private readonly IConnectionStringService _applicationSetting;

        public MinutzLocationService(IMinutzLocationRepository minutzLocationRepository, IConnectionStringService applicationSetting)
        {
            _minutzLocationRepository = minutzLocationRepository;
            _applicationSetting = applicationSetting;
        }
        
        public MessageBase Update(string meetingId, string location, AuthRestModel user, string schema)
        {
            var instanceConnectionString = _applicationSetting.CreateConnectionString(schema, _applicationSetting.GetInstancePassword(schema));
            return _minutzLocationRepository.Update(meetingId, location, user.InstanceId, instanceConnectionString);
        }
    }
 }
