using System;

namespace Minutz.Core.Api.Feature.Meeting
{
    public class MeetingAttachment
    {
        public Guid Id { get; set; }
        public Guid ReferenceId { get; set; }
        public string FileName { get; set; }
        public string MeetingAttendeeId { get; set; }
        public DateTime Date { get; set; }
        public byte[] FileData { get; set; }
        public int Order { get; set; }
    }
}