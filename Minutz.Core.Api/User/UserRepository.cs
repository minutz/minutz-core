using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.User
{
    public class UserRepository : IUserRepository
    {
        public Person GetUserByEmail(string email, string schema, string connectionString)
        {
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                var sql = $"select * FROM [{schema}].[Person] WHERE [Email] ='{email}'; ";
                try
                {
                    var query = dbConnection.Query<Person>(sql).ToList();
                    if (!query.Any()) return null;
                    var user = query.First();
                    return user;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }
            }
        }

        /// <summary>
        /// Check if the user exists in the Person table and instance Available 
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="meetingId"></param>
        /// <param name="schema"></param>
        /// <param name="connectionString"></param>
        /// <param name="masterConnectionString"></param>
        /// <returns></returns>
        public MessageBase CheckIfNewUser(string userEmail, string meetingId, string schema, string connectionString, string masterConnectionString)
        {
            // Check if the person is in the Person Table
            using (IDbConnection masterDbConnection = new SqlConnection(masterConnectionString))
            {
                var personSql = $"select * FROM [app].[Person]  WHERE [IdentityId] = '{userEmail}' ";
                masterDbConnection.Open();
                var personData = masterDbConnection.Query<Person>(personSql).FirstOrDefault();
                if (personData == null)
                    return new MessageBase
                    {
                        Condition = false,
                        Message = $"Person {userEmail} cannot be found",
                        Code = 1
                    };
                return new MessageBase{Message = "Found", Code = 2, Condition = true};
            }
        }
        
        /// <summary>
        /// Create a user in the person table.
        /// </summary>
        /// <param name="user" typeof="MeetingAttendee">The new user that needs to be added</param>
        /// <param name="masterConnectionString" typeof="string">The main database connection</param>
        /// <returns></returns>
        public MessageBase CreatePerson(MeetingAttendee user, string masterConnectionString)
        {
            using (IDbConnection masterDbConnection = new SqlConnection(masterConnectionString))
            {
                if (string.IsNullOrEmpty(user.Name))
                    user.Name = user.Email.Split('@')[0].Replace(".", " ").Substring(0, 1).ToUpper();
                masterDbConnection.Open();
                var insertQuery = $@"INSERT INTO [app].[Person]
                            ([IdentityId], [FirstName], [LastName], [FullName], [ProfilePicture], [Email], [Role], [Active], [InstanceId])
														 VALUES('{user.Email}', '{user.Name}', '{user.Name}', '{user.Name}', '{user.Picture}', '{user.Email}', '{user.Role}', 1, '{user.ReferenceId}')";
                var insertData = masterDbConnection.Execute(insertQuery);
                if (insertData == 1)
                {
                    return new MessageBase {Condition = true, Message = "Successful", Code = 200};
                }
                return new MessageBase{Condition = false, Message = $"The person {user.Email} could not be added.", Code = 500};
            }
        }
    }
}