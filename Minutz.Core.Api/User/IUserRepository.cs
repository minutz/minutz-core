using Minutz.Core.Api.Models.Entities;
using Minutz.Core.Feature.Person;

namespace Minutz.Core.Api.User
{
    public interface IUserRepository
    {
        Person GetUserByEmail(string email, string schema, string connectionString);

        MessageBase CheckIfNewUser(string userEmail, string meetingId, string schema, string connectionString, string masterConnectionString);

        MessageBase CreatePerson(MeetingAttendee user, string masterConnectionString);
    }
}