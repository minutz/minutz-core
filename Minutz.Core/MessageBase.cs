namespace Minutz.Core
{
    public class MessageBase
    {
        public int Code { get; set; }
        public bool Condition { get; set; }
        public string Message { get; set; }
    }
}