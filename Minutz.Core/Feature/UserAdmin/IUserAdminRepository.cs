using System.Data;
using System.Threading.Tasks;

namespace Minutz.Core.Feature.UserAdmin {
    public interface IUserAdminRepository {
        Task<UserAdminMessage> GetUsersAsync (IDbConnection connection, string schema);

        Task<UserAdminMessage> GetAvailableUsersAsync (IDbConnection connection, string schema);

        Task<UserAdminMessage> UpdateUserAsync (IDbConnection connection, InstanceUser user, string schema);

        Task<UserAdminMessage> AddUserAsync (IDbConnection masterConnection, InstanceUser user, string schema);

        Task<UserAdminMessage> DeleteUserAsync (IDbConnection masterConnection, InstanceUser user, string schema);
    }
}