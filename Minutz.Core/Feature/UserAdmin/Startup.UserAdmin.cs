using Microsoft.Extensions.DependencyInjection;

namespace Minutz.Core.Feature.UserAdmin
{
    public static class Startup_UserAdmin
    {
        public static void ConfigureUserAdminServices(this IServiceCollection services)
        {
            services.AddTransient<IUserAdminRepository, UserAdminRepository>();
        } 
    }
}