using Microsoft.Extensions.DependencyInjection;

namespace Minutz.Core.Feature.Related
{
    public static class Startup_PersonInstanceValidation
    {
        public static void ConfigurePersonInstanceValidationServices(this IServiceCollection services)
        {
            services.AddTransient<IPersonInstanceValidation, PersonInstanceValidation>();
        }
    }
}