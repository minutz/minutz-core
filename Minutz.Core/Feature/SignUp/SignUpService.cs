using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Minutz.Core.Feature.Http;
using Newtonsoft.Json;

namespace Minutz.Core.Feature.SignUp {
    public class SignUpService : ISignUpService {
        private readonly IHttpAccountService _httpAccountService;
        private string _signUpEndpoint => Environment.GetEnvironmentVariable ("AUTH_API_URL");

        public SignUpService (IHttpAccountService httpService) {
            _httpAccountService = httpService;
        }

        public async Task<MessageBase> SignUpUserAsync (string name, string instanceId, string email) {
            var userObject = new CreateUserViewModel {
                RefInstanceId = instanceId,
                email = email,
                password = GeneratePassword (),
                role = "User",
                username = email,
                name = name
            };
            var payload = new StringContent (JsonConvert.SerializeObject (userObject), Encoding.UTF8, "application/json");
            return await _httpAccountService.PostAsync ($"{_signUpEndpoint}api/SignUp", payload);
        }

        private static string GeneratePassword () {
            return "@FooBarIsAwesome001";
        }
    }
}