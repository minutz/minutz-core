using Microsoft.Extensions.DependencyInjection;

namespace Minutz.Core.Feature.Http {
    public static class Startup_HttpService {
        public static void ConfigureAdminHttpServiceServices (this IServiceCollection services) {
            services.AddTransient<IHttpAccountService, HttpAccountService> ();
        }
    }
}