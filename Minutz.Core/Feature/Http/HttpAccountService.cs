using System.Net.Http;
using System.Threading.Tasks;

namespace Minutz.Core.Feature.Http {
    public class HttpAccountService : IHttpAccountService {
        public async Task<MessageBase> GetAsync (string endpoint, string token) {
            var httpClient = new HttpClient ();
            httpClient.DefaultRequestHeaders.Add ("Authorization", token);
            var result = await httpClient.GetAsync (endpoint);
            return new MessageBase {
                Code = (int) result.StatusCode,
                    Condition = result.IsSuccessStatusCode,
                    Message = await result.Content.ReadAsStringAsync ()
            };
        }

        public async Task<MessageBase> PostAsync (string endpoint, StringContent body, string token) {
            var httpClient = new HttpClient ();
            httpClient.DefaultRequestHeaders.Add ("Authorization", token);
            var result = await httpClient.PostAsync (endpoint, body);
            return new MessageBase {
                Code = (int) result.StatusCode,
                    Condition = result.IsSuccessStatusCode,
                    Message = await result.Content.ReadAsStringAsync ()
            };
        }

        public async Task<MessageBase> PostAsync (string endpoint, StringContent body) {
            var httpClient = new HttpClient ();
            var result = await httpClient.PostAsync (endpoint, body);
            return new MessageBase {
                Code = (int) result.StatusCode,
                    Condition = result.IsSuccessStatusCode,
                    Message = await result.Content.ReadAsStringAsync ()
            };
        }
    }
}