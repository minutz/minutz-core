using System.Net.Http;
using System.Threading.Tasks;

namespace Minutz.Core.Feature.Http {
    public interface IHttpAccountService {

        Task<MessageBase> GetAsync (string endpoint, string token);

        Task<MessageBase> PostAsync (string endpoint, StringContent body, string token);

        Task<MessageBase> PostAsync (string endpoint, StringContent body);
    }
}