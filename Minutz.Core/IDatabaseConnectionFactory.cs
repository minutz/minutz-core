using System.Data;

namespace Minutz.Core
{
    public interface IDatabaseConnectionFactory
    {
        IDbConnection GetConnection();
    }
}